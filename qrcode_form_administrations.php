<?php


include_spip('base/upgrade');
 /**
 * Installation/maj du plugin `formulaire_qrcode`.
 * Pour l'heure, ne fait rien, mais permet d'avoir déjà un numéro en meta base
 * Si jamais un jour on a besoin de gérer de la BDD (je suis dubitatif)
 *
 * @param string $nom_meta_base_version
 * @param string $version_cible
 */
function qrcode_form_upgrade(string $nom_meta_base_version, string $version_cible): void {
	$maj = ['create' => []];
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Déinstallation de la meta
 * @param string $nom_meta_base_version
**/
function qrcode_form_vider_tables(string $nom_meta_base_version): void {
	effacer_meta($nom_meta_base_version);
}
