# Formulaire générateur de QRcode

Ce micro plugin permet d'insérer sur certaines pages un simple formulaire permettant de générer des QRCodes.

Il se base sur le plugin QRcode.

## DOCUMENTATION

Documentation : https://contrib.spip.net/5562

## LICENCE

GPL 3
