# Changelog

## 1.1.0 - 2024-11-16

### Fixed

- Correction de nom sur le fichier téléchargé

## 1.1.0 - 2024-11-16

### Added

- Paramètre `defaut` à l'appel du formulaire

### Fixed


- Réafficher le formulaire après soumission
- Ne pas afficher la ligne première ligne vide sur le choix du taux d'erreur
- Générer correctement le nom du téléchargement

## 1.0.0 - 2024-09-16

### Added

- Première sortie du plugin
