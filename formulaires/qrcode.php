<?php

/**
 * Retourne les saisies du qrcode
 * @param array $config tableau de config
 *		- `forcer_url` pour n'accepter que des urls
 *		- `defaut` tableau des valeurs par défaut
 * @return array tableau de saisies
 **/
function formulaires_qrcode_saisies(array $config = []): array {
	$forcer_url = $config['forcer_url'] ?? false;

	$saisies = [];
	if ($forcer_url) {
		$verifier_texte = [
			[
				'type' => 'url'
			]
		];
		$label_texte = '<:qrcode_form:url_label:>';
	} else {
		$verifier_texte = [];
		$label_texte = '<:qrcode_form:texte_label:>';
	}

	$saisies[] = [
		'saisie' => 'input',
		'options' => [
			'nom' => 'texte',
			'label' => $label_texte,
			'obligatoire' => 'on',
			'defaut' => $config['defaut']['texte'] ?? '',
		],
		'verifier' => $verifier_texte
	];

	$saisies[] = [
		'saisie' => 'input',
		'options' => [
			'nom' => 'taille',
			'label' => '<:qrcode_form:taille:>',
			'defaut' => $config['defaut']['taille'] ?? lire_config('qrcode/taille', 1),
			'obligatoire' => 'on',
		],
		'verifier' => [
			[
				'type' => 'entier',
				'options' => [
					'min' => 1
				]
			]
		]
	];

	$saisies[] = [
		'saisie' => 'selection',
		'options' => [
			'defaut' => $config['defaut']['ecc'] ?? lire_config('qrcode/ecc', 'L'),
			'nom' => 'ecc',
			'label' => '<:qrcode_form:ecc:>',
			'cacher_option_intro' => 'on',
			'data' => [
				'L' => 'L 7%',
				'M' => 'M 15%',
				'Q' => 'Q 25%',
				'H' => 'H 30 %',
			]
		]
	];

	return $saisies;
}

function formulaires_qrcode_traiter($config) {
	include_spip('inc/saisies');
	include_spip('qrcode_fonctions');

	$forcer_url = $config['forcer_url'] ?? false;

	$texte = saisies_request('texte');
	$taille = saisies_request('taille');
	$ecc = saisies_request('ecc');


	$png = qrcode_getpng($texte, $taille, $ecc);
	if (!$forcer_url) {
		$download = 'qrcode_' . attribut_html($texte);
		// Si on force l'url, le telechargement est la dernière partie de l'url
	} else {
		$parse_url = parse_url($texte);
		if (!($parse_url['path'] ?? '')) {
			$download = 'qrcode_' . attribut_html($parse_url['host']);
		} else {
			$path = pathinfo($parse_url['path']);
			$download = 'qrcode_' . attribut_html($path['filename']);
		}
	}
	$balise_img = charger_filtre('balise_img');
	return [
		'editable' => 'true',
		'message_ok' =>
			$balise_img($png)
			. "<br /> <a href='$png' download='$download'"
			. '>'
			. _T('qrcode_form:telecharger')
			. '</a>'
	];
}
